# Maintainer: Michael Lass <bevan@bi-co.net>

# This PKGBUILD heavily borrows from java-openjdk in [extra] maintained by:
# Levente Polyak <anthraxx[at]archlinux[dot]org>
# Guillaume ALAUX <guillaume@archlinux.org>

# This PKGBUILD is maintained on github:
# https://github.com/michaellass/AUR

_majorver=8
_completever=8u232
_updatever=b09
pkgrel=1
pkgver=${_completever}.u${_updatever}
_tag_ver=${_completever}+${_updatever}

pkgname=jdk8-adoptopenjdk
pkgdesc="OpenJDK Java ${_majorver} development kit (AdoptOpenJDK build)"
arch=('x86_64')
url='https://adoptopenjdk.net/'
license=('custom')

depends=('java-runtime-common>=3' 'ca-certificates-utils' 'desktop-file-utils' 'libxrender' 'libxtst' 'alsa-lib')
optdepends=('gtk2: for the Gtk+ 2 look and feel'
            'gtk3: for the Gtk+ 3 look and feel')
provides=("java-runtime-headless=${_majorver}"
          "java-runtime-headless-openjdk=${_majorver}"
          "jre${_majorver}-openjdk-headless=${pkgver}"
          "jre-openjdk-headless=${pkgver}"
          "java-runtime=${_majorver}"
          "java-runtime-openjdk=${_majorver}"
          "jre${_majorver}-openjdk=${pkgver}"
          "jre-openjdk=${pkgver}"
          "java-environment=${_majorver}"
          "java-environment-openjdk=${_majorver}"
          "jdk${_majorver}-openjdk=${pkgver}"
          "jdk-openjdk=${pkgver}"
          "openjdk${_majorver}-src=${pkgver}"
          "openjdk-src=${pkgver}")
backup=(etc/${pkgname}/logging.properties
        etc/${pkgname}/management/jmxremote.access
        etc/${pkgname}/management/jmxremote.password.template
        etc/${pkgname}/management/management.properties
        etc/${pkgname}/net.properties
        etc/${pkgname}/sdp/sdp.conf.template
        etc/${pkgname}/security/java.policy
        etc/${pkgname}/security/java.security
        etc/${pkgname}/security/policy/limited/default_local.policy
        etc/${pkgname}/security/policy/limited/default_US_export.policy
        etc/${pkgname}/security/policy/limited/exempt_local.policy
        etc/${pkgname}/security/policy/README.txt
        etc/${pkgname}/security/policy/unlimited/default_local.policy
        etc/${pkgname}/security/policy/unlimited/default_US_export.policy
        etc/${pkgname}/sound.properties)
install=install_jdk-adoptopenjdk.sh

source=(https://github.com/AdoptOpenJDK/openjdk${_majorver}-binaries/releases/download/jdk${_tag_ver/+/-}/OpenJDK${_majorver}U-jdk_x64_linux_hotspot_${_tag_ver/+/}.tar.gz
        freedesktop-java.desktop
        freedesktop-jconsole.desktop
        freedesktop-jshell.desktop)
sha256sums=('7b7884f2eb2ba2d47f4c0bf3bb1a2a95b73a3a7734bd47ebf9798483a7bcc423'
            'ffeb631f59b09c6e630aced8778fd9c34b9144b9d0ce565e859da9978b6ed5b5'
            '687a88000d78a763c09136720d70b091e083ad8428fa960beb9123b0798db961'
            '0fe281cd0df3e304ab52e5e4f3db07a26da5f86c3dbfa3da1c4001ccff6a545b')

_jvmdir=/usr/lib/jvm/java-${_majorver}-adoptopenjdk
_jdkdir=jdk${_completever}-${_updatever}

package() {

  install -dm 755 "${pkgdir}${_jvmdir}"
  cp -a "${srcdir}/${_jdkdir}"/* "${pkgdir}${_jvmdir}"

  cd "${pkgdir}${_jvmdir}"

  # Conf
  install -dm 755 "${pkgdir}/etc"
#  mv conf "${pkgdir}/etc/${pkgname}"
  ln -sf /etc/${pkgname} conf

  # Legal
  install -dm 755 "${pkgdir}/usr/share/licenses"
#  mv legal "${pkgdir}/usr/share/licenses/${pkgname}"
  ln -sf /usr/share/licenses/${pkgname} legal

  # Man pages
  for f in man/man1/*; do
    install -Dm 644 "${f}" "${pkgdir}/usr/share/${f/\.1/-adoptopenjdk${_majorver}.1}"
  done
  rm -rf man
  ln -sf /usr/share/man man

  # Link JKS keystore from ca-certificates-utils
#  rm -f lib/security/cacerts
#  ln -sf /etc/ssl/certs/java/cacerts lib/security/cacerts

  # Desktop files
  for f in jconsole java jshell; do
    install -Dm 644 \
      "${srcdir}/freedesktop-${f}.desktop" \
      "${pkgdir}/usr/share/applications/${f}-${pkgname}.desktop"
  done

}
